﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;


namespace Monitor_of_object_security
{
    class MonitorOfObjectSecurity
    {
        private Thread monitor;
        private Thread dateMonitoring;
        private bool Activated;
        private Shedule shedule;
        private bool ErrorOfAccess;

        public MonitorOfObjectSecurity(string[] shedule)
        {
            monitor = new Thread(Blocking);
            this.shedule = new Shedule(shedule);
            Activated = false;
        }

        public DateTime[] Show()
        {
            return shedule.ShowShedule();
        }

        public void LoadNewShedule(string[] shedule)
        {
            this.shedule = new Shedule(shedule);
        }

        public MonitorOfObjectSecurity(bool Activated)
        {
            this.Activated = Activated;
            monitor = new Thread(Blocking);
            monitor.Start();
        }

        void Blocking()
        {
            while (Activated)
            {
                foreach (Process process in Process.GetProcesses())
                {
                    if (process.ProcessName == "notepad" || process.ProcessName == "notepad++")
                    {
                        if (!shedule.IsInSchedule(DateTime.Now))
                        {
                            try
                            {
                                process.Kill();
                            }
                            catch
                            {
                                ErrorOfAccess = true;
                            }
                        }
                    }
                }
            }
        }

        public void Activate()
        {
            if (monitor.ThreadState == System.Threading.ThreadState.Stopped)
            {
                monitor = new Thread(Blocking);
            }
            Activated = true;
            monitor.Start();
        }

        public void Deactivate()
        {
            Activated = false;
        }

        public bool IsActivated()
        {
            return Activated;
        }

        public bool GetErrorOfAccessStatement()
        {
            return ErrorOfAccess;
        }
    }
}
