﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monitor_of_object_security
{
    class Shedule
    {
        private List<DateTime> listOfTimes;

        public Shedule()
        {
            listOfTimes = new List<DateTime>();
        }

        public Shedule(string[] Shedules)
        {
            listOfTimes = new List<DateTime>();
            LoadSheduleFrom(Shedules);
        }

        public bool LoadSheduleFrom(string[] Shedules)
        {
            string[] TempTime = null;
            bool Error = false;
            int Hours = 0;
            int Hours2 = 0;
            int Minutes = 0;
            int Minutes2 = 0;
            int Seconds = 0;
            int Seconds2 = 0;
            int Incr = 0;
            int Counter = 0;
            listOfTimes.Clear();
            foreach (string Time in Shedules)
            {
                TempTime = Time.Split(':');
                for (int i = 0; i < TempTime.Length; i++)
                {
                    for (int j = 0; j < TempTime[i].Length; j++)
                    {
                        if (TempTime[i][j] <= '9' && TempTime[i][j] >= '0')
                        {
                            if (i == 0)
                            {
                                try
                                {
                                    Hours = int.Parse(TempTime[i]);
                                }
                                catch
                                {
                                    Error = true;
                                    break;
                                }
                                if (Hours < 0 || Hours > 23)
                                {
                                    Error = true;
                                    break;
                                }
                            }
                            else
                            {
                                try
                                {
                                    if (i == 1)
                                    {
                                        Minutes = int.Parse(TempTime[i]);
                                    }
                                    if (i == 2)
                                    {
                                        Seconds = int.Parse(TempTime[i]);
                                    }
                                }
                                catch
                                {
                                    Error = true;
                                    break;
                                }
                                if (Minutes < 0 || Minutes > 59 || Seconds < 0 || Seconds > 59)
                                {
                                    Error = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            Error = true;
                            break;
                        }
                    }
                    if (Error)
                    {
                        break;
                    }
                }
                if (Error)
                {
                    listOfTimes.Clear();
                    break;
                }
                if (Counter % 2 == 0)
                {
                    Hours2 = Hours;
                    Minutes2 = Minutes;
                    Seconds2 = Seconds;
                    listOfTimes.Add(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Hours, Minutes, Seconds));
                }
                else
                {
                    if (Hours2 > Hours || Minutes2 > Minutes || Seconds2 > Seconds)
                    {
                        Incr = 1;
                    }
                    else
                    {
                        Incr = 0;
                    }
                    listOfTimes.Add(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + Incr, Hours, Minutes, Seconds));
                }
                Counter++;
            }
            return Error;
        }

        public void Add(DateTime StartDate, DateTime FinishDate)
        {
            listOfTimes.Add(StartDate);
            listOfTimes.Add(FinishDate);
        }

        public DateTime[] ShowShedule()
        {
            return listOfTimes.ToArray();
        }

        public bool IsInSchedule(DateTime date)
        {
            bool Result = false;
            for (int i = 0; i < listOfTimes.Count(); i += 2)
            {
                if(date >= listOfTimes[i] && date <= listOfTimes[i+1]){
                    Result = true;
                }
            }
            return Result;
        }
    }
}
