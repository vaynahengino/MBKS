﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace Monitor_of_object_security
{
    public partial class Form1 : Form
    {
        private MonitorOfObjectSecurity monitor;
        bool IsChangedShedule;
        char[] Separators;

        public Form1()
        {
            InitializeComponent();
            List<char> SeparatorsTemp = Environment.NewLine.ToCharArray().ToList<char>();
            SeparatorsTemp.Add('-');
            this.Separators = SeparatorsTemp.ToArray();
            SeparatorsTemp.Clear();
            textBoxShedule.Text = textBoxShedule.Text.Trim(Separators);
            this.ShowInTaskbar = false;
            this.IsChangedShedule = false;
            monitor = new MonitorOfObjectSecurity(textBoxShedule.Text.Split(Separators));
        }

        private void buttonActivityOfMonitor_Click(object sender, EventArgs e)
        {
            if (buttonActivityOfMonitor.Text == "Активировать монитор")
            {
                textBoxShedule.Text = textBoxShedule.Text.Trim(Separators);
                buttonActivityOfMonitor.Text = "Деактивировать монитор";
                textBoxShedule.Enabled = false;
                if (IsChangedShedule)
                {
                    monitor.LoadNewShedule(textBoxShedule.Text.Split(Separators));
                    IsChangedShedule = false;
                }
                monitor.Activate();
            }
            else
            {
                monitor.Deactivate();
                buttonActivityOfMonitor.Text = "Активировать монитор";
                textBoxShedule.Enabled = true;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            monitor.Deactivate();
            notifyIcon1.Visible = false;
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (this.Visible == false)
            {
                this.Show();
                this.GetTopLevel();
            }
            else
            {
                this.Hide();
            }
        }

        private void textBoxShedule_TextChanged(object sender, EventArgs e)
        {
            IsChangedShedule = true;
        }
    }
}
